//
//  Protocols.swift
//  Contacts List Realtime Chat Application
//
//  Created by Alvin Jake Jebulan on 10/12/2019.
//  Copyright © 2019 ios developer. All rights reserved.
//

import UIKit
import Result

public typealias UserRequestCompletionBlock = (Result<[User], TaskError>) -> ()
public typealias UserFetchCompletionBlock = (Result<[UserProtocol], TaskError>) -> ()



public protocol APIFetchProtocol {
    func fetchRemoteUserDetails(completion: @escaping UserRequestCompletionBlock)
}

public enum TaskError: Error {
    case Saving(String)
    case Fetching(String)
}
