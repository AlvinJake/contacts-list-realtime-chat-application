//
//  RemoteAPI.swift
//  Contacts List Realtime Chat Application
//
//  Created by Alvin Jake Jebulan on 10/12/2019.
//  Copyright © 2019 ios developer. All rights reserved.
//

import UIKit
import Result
import FirebaseFirestore

class FirebaseAPI: NSObject {

var userResults: [User] = []

func fetchRemoteUser(completion: @escaping UserRequestCompletionBlock) {
    

        let db = Firestore.firestore()
        let collectionRef = db.collection("sampleData")
        collectionRef.getDocuments { (querySnapshot, err) in
            if let users = querySnapshot?.documents {
                for userList in users {
                    
                    
                    let email = userList.get("Email") as? String
                    let username = userList.get("Username") as? String
                    
                    
                    
                    let userValues: User = User(name: username ?? "user name", email: email ?? "email")
                    
                    self.userResults.append(userValues)
            }
        }
    }
                DispatchQueue.main.async {
                    
                    completion(Result.success(self.userResults))
                    
                    
                }
                
            }
        }

