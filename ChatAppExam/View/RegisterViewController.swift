//
//  RegisterViewController.swift
//  Contacts List Realtime Chat Application
//
//  Created by RAlvin Jake Jebulan on 10/12/2019.
//  Copyright © 2019 ios developer. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseFirestore



class RegisterViewController: UIViewController {
    
    
    var ref: DocumentReference!
   
    
    
    
    @IBOutlet weak var UsernameField: UITextField!
    @IBOutlet weak var EmailField: UITextField!
    @IBOutlet weak var PasswordField: UITextField!
    @IBOutlet weak var PasswordConfirm: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    ref = Firestore.firestore().document("sampleData/Users")
        
    }
    
    @IBAction func registerUser() {
        
        let userName: String = self.UsernameField.text!
        let emailField: String = self.EmailField.text!
        let passwordField: String = self.PasswordField.text!
        let passwordconfirm: String = self.PasswordConfirm.text!
        
        
        
        
        if passwordField != passwordconfirm {
            let alertController = UIAlertController(title: "Password Incorrect", message: "Please re-type password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            
            createUser(userName: userName, email: emailField, password: passwordField)
        }
    }

    func createUser(userName: String, email: String, password: String){
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let err = error {
                print("there was an error", err)
            }
        else {
            if let currentUSer = Auth.auth().currentUser?.createProfileChangeRequest(){
                
                currentUSer.displayName = userName
                
                currentUSer.commitChanges { (error) in
                    if let e = error{
                        print("there was an error", e)
                    }else{
                        
                        guard let users = self.UsernameField.text, !users.isEmpty else { return }
                        guard let emails = self.EmailField.text, !emails.isEmpty else { return }
                        
                   
                        let dataToSave: [String: Any] = ["Username": users, "Email": emails]
                        self.ref.setData(dataToSave) { (error) in
                            if let error = error {
                                print("Oh no! Error: \(error.localizedDescription)")
                            }else{
                                print("Data has been saved!")
                            }
                        }
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "Home") as! UIViewController
                        self.present(viewController, animated: true, completion: nil)
                        print("Create account success")
                    }
                }
            }
        }
}
}
}
