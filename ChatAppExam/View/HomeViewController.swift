//
//  HomeViewController.swift
//  Contacts List Realtime Chat Application
//
//  Created by Alvin Jake Jebulan on 10/12/2019.
//  Copyright © 2019 ios developer. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase





class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var ref: DocumentReference!
    let userReq = FirebaseAPI()
    var userDetails: [User]!
    
    @IBOutlet weak var userCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.userCollectionView.dataSource = self
        self.userCollectionView.delegate = self
        
        userDetails = [User]()
        fetchData()

    }
    
    func fetchData() {
        userReq.fetchRemoteUser { (result) in
            switch result {
            case .success(let data):
                print("check")
                self.userDetails = data
            case let .failure(error):
                print(error)
            }
            self.reloadUser(userDetails: self.userDetails)
        }
    }
    


}
extension HomeViewController {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.userDetails.count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userViews",
                                                      for: indexPath) as! userCollectionViewCell
        let cellModel = self.userDetails[indexPath.row]
        cell.emailAddress.text = cellModel.email
        cell.fieldName.text = cellModel.name

        return cell
    }
}

extension HomeViewController {
    func reloadUser(userDetails: [User]) {
        self.userDetails = userDetails
        self.userCollectionView.reloadData()
    }
}
