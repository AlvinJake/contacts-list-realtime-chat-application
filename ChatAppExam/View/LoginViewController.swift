//
//  LoginViewController.swift
//  Contacts List Realtime Chat Application
//
//  Created by RAlvin Jake Jebulan on 10/12/2019.
//  Copyright © 2019 ios developer. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class LoginViewController: UIViewController {
    
    @IBOutlet var EmailField: UITextField!
    @IBOutlet var PasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func loginAction(_ sender: Any) {
        
                let emailField: String = self.EmailField.text!
                let passwordField: String = self.PasswordField.text!
        
                Auth.auth().signIn(withEmail: emailField, password: passwordField) { (user, error) in
                    if error == nil{
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "Home") as! UIViewController
                        self.present(viewController, animated: true, completion: nil)
                    }
                    else{
                        let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
        
            }

    

}
