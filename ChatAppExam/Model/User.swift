//
//  User.swift
//  Contacts List Realtime Chat Application
//
//  Created by Alvin Jake Jebulan on 10/12/2019.
//  Copyright © 2019 ios developer. All rights reserved.
//

import UIKit

public protocol UserProtocol {
    var name: String { get set }
    var email: String { get set }

}

public struct User: UserProtocol {
    public var name: String
    public var email: String

    
    public init(name: String, email: String) {
        self.name = name
        self.email = email

    }
}

